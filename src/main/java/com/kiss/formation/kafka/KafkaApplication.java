package com.kiss.formation.kafka;

import com.kiss.formation.kafka.producer.SimpleProducer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.ExecutionException;

@SpringBootApplication
public class KafkaApplication {

	public static void main(String[] args) throws ExecutionException, InterruptedException {
		ConfigurableApplicationContext run = SpringApplication.run(KafkaApplication.class, args);
		run.getBean(SimpleProducer.class).produce();
	}
}
