package com.kiss.formation.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Create topic before (if auto-create disabled).
 * kafka-topics --zookeeper localhost:2181 --create --topic topic_1 --partitions 1 --replication-factor 1
 * Delete topic command
 * kafka-topics --zookeeper localhost:2181 --delete --topic topic_1
 */
@Component
public class SimpleProducer {

    public void produce() throws ExecutionException, InterruptedException {
        // create instance for properties to access producer configs
        Properties props = new Properties();

        // Assign localhost id
        props.put("bootstrap.servers", "localhost:9092");

        // Set acknowledgements for producer requests.
        props.put("acks", "all");

        // If the request fails, the producer can automatically retry,
        props.put("retries", 0);

        // Define serializers
        // "org.apache.kafka.common.serialization.StringSerializer"
        props.put("key.serializer",
                StringSerializer.class.getName());

        props.put("value.serializer",
                StringSerializer.class.getName());

        try (Producer<String, String> producer = new KafkaProducer<>(props)) {

            String topicName = "topic_1";
            long start = System.currentTimeMillis();
            for (int i = 0; i < 100; i++)
                producer.send(new ProducerRecord<>(topicName,
                        Integer.toString(i), Integer.toString(i))).get();
            long time = System.currentTimeMillis() - start;
            System.out.println("All messages sent in " + time + " ms");
        }
    }

}
